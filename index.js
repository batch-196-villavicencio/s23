console.log("Hello, World");

let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu","Charizard","Squirtle","Bulbasaur"],
	friends: {
		hoenn: ["May","Max"],
		kanto: ["Brock","Misty"]
	},
	talk: function(){
		console.log("Pikachu! I choose you!");
	}
}
console.log(trainer);
console.log("Result of dot notation:");
console.log(trainer.name);
console.log("Result of square bracket notation:");
console.log(trainer.pokemon);
console.log("Result of talk method");
trainer.talk();

function Pokemon(name,level){
	this.name = name;
	this.level = level;
	this.health = level * 3;
	this.attack = level * 1.5;
	this.tackle = function(enemy){
		enemy.health = enemy.health - this.attack;
		console.log(this.name + " tackled " + enemy.name);
		console.log(enemy.name + "\'s health is now reduced to " + enemy.health);
		if (enemy.health < 1){
			enemy.faint();
		}
	};
	this.faint = function(){
		console.log(this.name + " has fainted");
	};
};

let pokemon1 = new Pokemon("Pikachu",12);
console.log(pokemon1);
let pokemon2 = new Pokemon("Geodude",8);
console.log(pokemon2);
let pokemon3 = new Pokemon("Mewtwo",100);
console.log(pokemon3);
pokemon2.tackle(pokemon1);
console.log(pokemon1);
pokemon3.tackle(pokemon2);
console.log(pokemon2);